import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

//Main.java; Compiled onto one file for easy correction
//Person who taught me the entirety of this code: Jeremy L.
//Author: Mani B.

enum ETeam
{
    Blue,
    Green,
}

enum EGameState
{
    Selecting,
    Moving,
}

class GamePiece
{
    public ETeam Team;
    private Point Pos;
    public boolean IsKing;

    GamePiece(ETeam Team, int x, int y)
    {
        this.Team = Team;
        Pos = new Point(x, y);
    }

    public boolean CanMoveToPos(int x, int y)
    {
        if(Main.DistanceTo(x, y, Pos.x, Pos.y) <= 2 && Main.Squares[x][y].IsEmpty()) {
            return !(y - Pos.y < 0) && Team == ETeam.Blue || IsKing || !(Pos.y - y < 0) && Team == ETeam.Green || IsKing;
        }
        else if(Main.DistanceTo(x, y, Pos.x, Pos.y) <= 3 && Main.Squares[x][y].IsEmpty())
        {
            Point mid = Main.GetMidpoint(x, y, Pos.x, Pos.y);
            return !Main.Squares[mid.x][mid.y].IsEmpty();
        }
        else
        {
            return false;
        }
    }

    public boolean MovePiece(int x, int y)
    {
        if(!CanMoveToPos(x, y))
            return false;
        if(Main.Squares[x][y] == null)
            return false;
        if(!Main.Squares[x][y].IsEmpty())
            return false;

        if(Main.DistanceTo(x, y, Pos.x, Pos.y) <= 2)
        {
            Main.Squares[x][y].SetCurrentPiece(this);
            Main.Squares[Pos.x][Pos.y].Empty();
            Pos.x = x;
            Pos.y = y;
            return true;
        }
        else if(Main.DistanceTo(x, y, Pos.x, Pos.y) <= 3)
        {
            Point mid = Main.GetMidpoint(x, y, Pos.x, Pos.y);
            if(Main.Squares[mid.x][mid.y].IsEmpty() || Main.Squares[mid.x][mid.y].GetCurrentPiece().Team == Main.CurrentUser)
            {
                return false;
            }
            else
            {
                Main.Squares[x][y].SetCurrentPiece(this);
                Main.Squares[Pos.x][Pos.y].Empty();
                GamePiece other = Main.Squares[mid.x][mid.y].GetCurrentPiece();

                if(Main.CurrentUser == ETeam.Blue)
                {
                    Main.RedCollected.add(other);
                    Main.BlackPieces.remove(other);
                }
                else
                {
                    Main.BlackCollected.add(other);
                    Main.RedPieces.remove(other);
                }

                Main.Squares[mid.x][mid.y].Empty();

                other.Pos.x = -1;
                other.Pos.y = -1;

                Pos.x = x;
                Pos.y = y;

                return true;
            }
        }
        else
        {
            return false;
        }
    }
}

class CheckersWindow extends Frame
{
}

class SquareClickListener implements ActionListener
{
    public GameSquare Square;

    public SquareClickListener(GameSquare sq)
    {
        Square = sq;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(Main.GameState == EGameState.Selecting) {
            if (!Square.IsEmpty()) {
                if (Square.GetCurrentPiece().Team == Main.CurrentUser) {
                    if(JOptionPane.showConfirmDialog(null, "Are you sure you want to select this piece?") == 0)
                    {
                        Square.setForeground(Color.BLUE);
                        JOptionPane.showMessageDialog(null, "Selected piece at: " + Square.XPos + ", " + Square.YPos);
                        Main.SelectedPiece = Square.GetCurrentPiece();
                        Main.ProgressGameState();
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "You may now select another piece...");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "You can't select a piece of the other team!");
                }
            } else {
                JOptionPane.showMessageDialog(null, "That square is empty!");
            }
        }
        else
        {
            if(Square.IsEmpty())
            {
                if(Main.SelectedPiece == null)
                    JOptionPane.showMessageDialog(null, "ERROR");
                if(Main.SelectedPiece.CanMoveToPos(Square.XPos, Square.YPos))
                {
                    if(JOptionPane.showConfirmDialog(null, "Are you sure you would like to move your piece to " + Square.XPos + ", " + Square.YPos + "?") == 0)
                    {
                        if(Main.SelectedPiece.MovePiece(Square.XPos, Square.YPos))
                        {
                            JOptionPane.showMessageDialog(null, "YAY IT MOVED!");
                            Main.ProgressGameState();
                            Square.repaint();
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Failed to move piece! D:");
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "You may now select another move position...");
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "That is out of this piece's range!");
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null, "That square is not empty!");
            }
        }
    }
}

class GameSquare extends Button
{
    @NotNull
    private Color color;
    @NotNull
    private Color pieceColor;

    @NotNull
    private Frame GameWindow;

    public int XPos;
    public int YPos;

    GamePiece CurrentPiece;

    public GameSquare(Color color, int x, int y, Frame wnd)
    {
        this.color = color;

        setBackground(color);

        pieceColor = Color.BLACK;

        XPos = x;
        YPos = y;

        this.addActionListener(new SquareClickListener(this));
    }

    public void SetColor(Color c) { color = c; }
    public Color GetColor() { return color; }

    /*
    WARNING: CAN RETURN NULL
     */
    @Nullable
    public GamePiece GetCurrentPiece()
    {
        return CurrentPiece;
    }

    public void SetCurrentPiece(@NotNull GamePiece pc)
    {
        CurrentPiece = pc;
        if(pc.Team == ETeam.Blue)
            pieceColor = Color.blue;
        else
            pieceColor = Color.green;
    }

    public void Empty()
    {
        CurrentPiece = null;
    }

    public boolean IsEmpty()
    {
        return CurrentPiece == null;
    }

    @Override
    public void paint(Graphics g) 
    {
        super.paint(g);
        g.setPaintMode();
        if(!IsEmpty())
        {
            setForeground(pieceColor);
            g.fillOval(10, 10, getWidth() - 20, getHeight() - 20);
        }

    }
}

public class Main
{
    public static GameSquare[][] Squares = new GameSquare[8][8];
    public static ArrayList<GamePiece> RedPieces = new ArrayList<GamePiece>();
    public static ArrayList<GamePiece> RedCollected = new ArrayList<GamePiece>();
    public static ArrayList<GamePiece> BlackPieces = new ArrayList<GamePiece>();
    public static ArrayList<GamePiece> BlackCollected = new ArrayList<GamePiece>();
    public static ETeam CurrentUser = ETeam.Blue;
    public static GamePiece SelectedPiece;
    public static EGameState GameState = EGameState.Selecting;
    public static void main(String[] argsss)
    {
        Frame GameWindow = new Frame("Checkers v0.0.0.0.0.0.0.0.0.1");
        GameWindow.setBounds(GameWindow.getX(), GameWindow.getY(), 800, 800);
        GameWindow.setLayout(new GridLayout(8, 8));
        PopulateSquares(GameWindow);
        PopulatePieces(Squares);
        GameWindow.setVisible(true);
        JOptionPane.showMessageDialog(null, "It is blue's turn first!");
        while(true)
        {
            GameWindow.repaint();
            CheckKings();
            EndGame();
            try
            {
                Thread.sleep(10);
            }
            catch(Exception e)
            {

            }
        }
    }

    public static void PopulatePieces(GameSquare[][] Squares)
    {
        for(int x = 0; x < 8; x++)
        {
            for(int y = 0; y < 8; y++)
            {
                if(y == 0)
                {
                    if(x % 2 != 0)
                    {
                        GamePiece p = new GamePiece(ETeam.Blue, x, y);
                        Squares[x][y].SetCurrentPiece(p);
                        RedPieces.add(p);
                    }
                }
                else if(y == 1)
                {
                    if(x % 2 == 0)
                    {
                        GamePiece p = new GamePiece(ETeam.Blue, x, y);
                        Squares[x][y].SetCurrentPiece(p);
                        RedPieces.add(p);
                    }
                }
                else if(y == 6)
                {
                    if(x % 2 == 0)
                    {
                        GamePiece p = new GamePiece(ETeam.Green, x, y);
                        Squares[x][y].SetCurrentPiece(p);
                        BlackPieces.add(p);
                    }
                }
                else if(y == 7)
                {
                    if(x % 2 != 0)
                    {
                        GamePiece p = new GamePiece(ETeam.Green, x, y);
                        Squares[x][y].SetCurrentPiece(p);
                        BlackPieces.add(p);
                    }
                }
            }
        }
    }

    public static void PopulateSquares(Frame wind)
    {
        for(int x = 0; x < 8; x++)
        {
            for(int y = 0; y < 8; y++)
            {
                if(y % 2 == 0)
                {
                    if(x % 2 == 0)
                    {
                        GameSquare s = new GameSquare(Color.RED, x, y, wind);
                        Squares[x][y] = s;
                        wind.add(s);
                    }
                    else
                    {
                        GameSquare s = new GameSquare(Color.BLACK, x, y, wind);
                        Squares[x][y] = s;
                        wind.add(s);
                    }
                }
                else
                {
                    if(x % 2 == 0)
                    {
                        GameSquare s = new GameSquare(Color.BLACK, x, y, wind);
                        Squares[x][y] = s;
                        wind.add(s);
                    }
                    else
                    {
                        GameSquare s = new GameSquare(Color.RED, x, y, wind);
                        Squares[x][y] = s;
                        wind.add(s);
                    }
                }
            }
        }
    }

    public static double DistanceTo(int x1, int y1, int x2, int y2)
    {
        return Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2 - y1, 2));
    }

    public static Point GetMidpoint(int x1, int y1, int x2, int y2)
    {
        return new Point((x1 + x2) / 2, (y1 + y2) / 2);
    }

    public static void EndGame()
    {
        if(RedPieces.isEmpty())
        {
            JOptionPane.showConfirmDialog(null, "Green wins!");
            try
            {
                Thread.sleep(2000);
                System.exit(666);
            }
            catch(Exception e)
            {

            }
        }
        else if(BlackPieces.isEmpty())
        {
            JOptionPane.showConfirmDialog(null, "Blue wins!");
            try
            {
                Thread.sleep(2000);
                System.exit(666);
            }
            catch(Exception e)
            {

            }
        }
    }

    public static void CheckKings()
    {
        for(int x = 0; x < 8; x++)
        {
            if(!Squares[x][0].IsEmpty() && Squares[x][0].GetCurrentPiece().Team == ETeam.Green)
                Squares[x][0].GetCurrentPiece().IsKing = true;
            if(!Squares[x][7].IsEmpty() && Squares[x][7].GetCurrentPiece().Team == ETeam.Blue)
                Squares[x][7].GetCurrentPiece().IsKing = true;
        }
    }

    public static void ProgressGameState()
    {

        if(GameState == EGameState.Moving)
        {
            GameState = EGameState.Selecting;
            if(CurrentUser == ETeam.Blue)
            {
                CurrentUser = ETeam.Green;
                JOptionPane.showMessageDialog(null, "It is now Green's turn to play!");
            }
            else
            {
                CurrentUser = ETeam.Blue;
                JOptionPane.showMessageDialog(null, "It is now Blue's turn to play!");
            }
        }
        else
            GameState = EGameState.Moving;
    }
}
